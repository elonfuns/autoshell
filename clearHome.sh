#!/bin/bash
source $HOME/autoshell/log.sh

#1.清理桌面--完成
cd $HOME/Desktop
mv *.mp3 *.m4a $HOME/Music/网易云音乐/
mv *.docx *.doc *.xls *.xlsx *.pdf $HOME/pm/
rm -rf *截*.* *屏*.* *快照*.* *选区*.* *屏*.*

#下载目录归档完成
cd $HOME/dl
zip $daynum.zip ./*.mp4
github=`ls $HOME/dl/ | grep master.zip`
for filename in ${github[@]}; do
	unzip $filename -d $HOME/code/
done
unrar e *.rar
mv *.mp3 *.mov $HOME/Music/网易云音乐/
mv *.docx *.doc *.xls *.xlsx *.pdf $HOME/pm/
mv *.dmg *.app $HOME/autoshell/app/
mv $daynum.zip $HOME/Movies/
mv *shell*.*  *bash*.* *inux*.* *操作系统*.* $HOME/docs/ebook/linux/
mv *数据*.* *sql*.* *SQL*.* *Sql*.* *edis** $HOME/docs/ebook/dbsql/
mv *ava?cript.* *JS*.*  $HOME/docs/ebook/javascript/
mv *.* $HOME/docs/ebook/	
rm -rf *_files *.html *-master.zip *.rar *.mp4

tenday=`expr $daynum % 10`;
if [ $tenday -eq 1 ]; then
	#mac 已安计软件备份
	echo $daynum >> $HOME/autoshell/app.conf
	ls /Applications/ |nl > $HOME/autoshell/app.conf
	echo | brew list |nl >> $HOME/autoshell/app.conf

	#crontab 备份
	crontab -l > crontab.sh

	#autoshell 自动提交git 
	cd $HOME/autoshell
	git pull
	sleep 20
	git commit -am "日常备份"
	git push
	sleep 30

	cd $HOME/env
	git pull
	sleep 20
	git commit -am "日常备份"
	git push
	sleep 30
fi

rm -rf $HOME/.Trash
