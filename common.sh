#!/bin/zsh
daynum=`date +%Y%m%d`;
fix=`uname`;
function log(){
	logtxt=" $daynum #####>>>>>> << $0 >> !!!!"
	echo "$logtxt" >> $HOME/autoshell/log.txt
}
# 文件加密
function encript(){
	newname=`echo $1|md5`
	zippwd="mypassword"
	savedir="./"
	if [ "$#" -eq 2 ] ; then
		savedir=$2;
	fi
	zip --password $zippwd $1.zip $1;
	mv $1.zip $savedir$newname.jpg;
}

#目录下所有文件encript poco.html $HOME/code/
function direncript(){
	`ls $1 > $HOME/autoshell/swap.txt`;
	for fname in `cat $HOME/autoshell/swap.txt`;
	do
		encript $fname $2
	done 
	# `rm -f $HOME/autoshell/swap.txt`;
}
# direncript $HOME/docs/person/ $HOME/docs/person/

# 目录下所有文件， tar 打包， 然后 xz 压缩
function dirxz(){
	dirs=`ls -l $1 |grep "^d" |awk '{print $9}'`;
	for dname in ${dirs[@]}; do
		tar cvf $dname.tar $dname;
		sleep 90
		xz -z $dname.tar;
	done 
}
# 	dirxz $HOME/pm/

# 直接调用查看变量类型
# demo
# 	$(vartype $1);
function vartype(){
    local a="$1"
    printf "%d" "$a" &>/dev/null && echo "integer" && return
    printf "%d" "$(echo $a|sed 's/^[+-]\?0\+//')" &>/dev/null && echo "integer" && return
    printf "%f" "$a" &>/dev/null && echo "number" && return
    [ ${#a} -eq 1 ] && echo "char" && return
    echo "string"
}
# vartype

#遍历文件夹及其子文件夹内所有文件，并查看各个文件大小
function getallfile()
{   
	rm -f $HOME/autoshell/swap.txt
    for element in `ls $1`; do  
        file=$1"/"$element
        if [ -d $file ]; then 
            getallfile $file
        else
            echo $file 1>> $HOME/autoshell/swap.txt
        fi  
    done
}
# getallfile $HOME/pm

# 文件大小过滤
# 按大小过滤文件，单位B,M=1024k*1024b;
# demo:
#	filefilter 41380224
function filefilter()
{
	for line in `cat $HOME/autoshell/swap.txt`;
	do
	    filesize=`ls -l $line | awk '{ print $5 }'`  #读取文件大小
	    if [ $filesize -gt $1 ]; then 
	    	echo $line >> $HOME/autoshell/filter.txt
	    	#查看文件实际大小
	    	# expr $filesize / 1024 / 1024 >> $HOME/autoshell/filter.txt
	    fi
	done
}
# filefilter 41380224

# 个人博客更新文章
function page(){
	ptl=$1;
	type=$2;
	`curl -O http://dm.com/"${ptl}"`;
	`curl -O http://dm.com/"${type}"`;
	`curl -O http://dm.com/index`;
	sed -e 's/dm.com/datamoney.net/' $ptl  > $HOME/code/page/"${ptl}".html;
	sed -e 's/dm.com/datamoney.net/' $type > $HOME/code/page/"${type}"/index.html;
	sed -e 's/" class="content-subhead"/.html" class="content-subhead"/' $type > $HOME/code/page/"${type}"/index.html;
	sed -e 's/dm.com/datamoney.net/' index > $HOME/code/page/index.html;
	sed -e 's/" class="content-subhead"/.html" class="content-subhead"/' index > $HOME/code/page/index.html;
	rm $type $ptl index;
}
# page Linux-Redis-在Redis设置一个空间即目录或直接独立库内专用于键空通知事件 data

# 更新全端-请注释目录
function intpage(){
	for ptl in `cat  $HOME/autoshell/swap.txt`;
	do
		page $ptl data
	done;
}
# filemv $HOME/code/ThinkAdmin/application/admin/view edit
function filemv(){
    for element in `ls $1`; 
    do  
        dir=$1"/"$element;
        if [ -d $dir ]; then 
			ls $dir | grep $2.html
			if [ $? -eq 0 ]; then
			 	cp $dir/$2.html $dir/form.html		
			fi
        fi
	done;
}
#获取项目忽略文件配置
# demo: 
# 	getignore $HOME/pm
function getignore(){
	file=$1"/".gitignore
	for line in `cat ${file}`;
	do
	    echo $1"/"$line >> $HOME/autoshell/ignore.txt
	done
}

#github上传大于40M文件自动检查和拆分
# params:
# 	$1= dir
# 	$2= filesizelimit
# demo:
#	gitcheck $HOME/pm 41380224
function gitcheck(){
	# date >> $HOME/autoshell/swap.txt
	#查找的目录生成
	dir=$1
	#按大小过滤文件，单位B,M=1024k*1024b;
	size=$(expr $2 \* 1024 \* 1024)

	print "正在遍历文件夹..."
	getallfile $dir
	
	print "过滤大于"$1"的文件..."
	filefilter $size
	
	print "获取当前项目中的.gitignore配置文件"
	getignore $dir

	print "对比大文件与.gitignore配置差异"
	cat $HOME/autoshell/filter.txt $HOME/autoshell/ignore.txt | sort | uniq > union.txt
	cat $HOME/autoshell/union.txt $HOME/autoshell/ignore.txt | sort | uniq -u >> $dir/.gitignore
	print "successful!! 任务完成！\n 文件在项目根目录查看gitignore"
	rm -rf $HOME/autoshell/swap.txt $HOME/autoshell/ignore.txt $HOME/autoshell/filter.txt 
}
# gitcheck $HOME/pm 40








